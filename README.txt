*************************
*    README CONTENTS    *
*************************

I. Description of files
II. How to run code
III. Copyright & Licensing
IV. References
V. Contact info


------------------------------
  I. Description of files
------------------------------

Below is a brief description of individual scripts:

    (i) find_nsclc_vulner.py
	This is a Python script that uses k-means clustering to calculate degree of 
	bimodal response of cell lines to loss of biological systems. It requires a 
	file with a table of cell line sensitivities to RNAi gene knockdown. The 
	user will need to provide an input file containing a list of gene sets. Both
	files are described in further detail below. The result of the script 
	produces a text file in which each line contains a gene set, its score from 
	the k-means clustering, and a p-value determined by a permutation test. 

    (ii) nsclc_qvalue.R
	This is a short R script that uses the QVALUE statistical package [1] to 
	perform multiple hypothesis correction on the p-values calculated from the 
	find_nsclc_vulner.py script. It takes an input file containing a list of 
	p-values and produces an output file with q-values and corresponding 
	significance calls. 

    (iii) plot_nsclc_vulner.py
	This is a Python script that plots the RNAi sensitivity profile of each 
	statistically significant genetic vulnerability determined from the 
	preceding two scripts above. It requires the user to provide an FDR level, 
	the list of gene sets (same input file to find_nsclc_vulner.py), and the 
	output file from find_nsclc_vulner.py.

	All programs were run using Python 3.4.1 in Anaconda 2.1 64-bit and R 
	version 3.1.2. The operating system was Ubuntu 14.04 LTS.

Below is a brief description of input files:

    (i) sample_input_data_file.txt
	Each column has a cell line that was screened with an Ambion RNAi library 
	("<cell line>.A") or a Dharmacon library ("<cell line>.D"). A gene appears 
	on each row, and the numerical values represent the viability score of a 
	cell line upon gene knockdown. In place of the original RNAi knockdown data 
	file, we provide here sample_input_data_file.txt, which is a synthetic 
	dataset with the same format as the actual data. Most of the values in this 
	synthetic dataset are drawn uniformly from [-10, 10], although there is 
	some block structure built-in. 30 groups of rows have between 4 and 7 
	columns all with values of -10 with the remaining columns equal to 0. The 
	filename "sample_input_data_file.txt" is hard-coded into the script 
	find_nsclc_vulner.py.

	(ii) sample_gene_set_file.txt
	     corum_havig.txt
	These files contain lists of gene sets and are to provided as command-line 
	arguments. The file corum_havig.txt contains protein complexes that were 
	examined in the actual RNAi dataset for evidence of lung cancer genetic 
	vulnerabilities. For the purposes of the study, an additional unpublished 
	dataset of protein complexes was analyzed along with those in 
	corum_havig.txt. The file sample_gene_set_file.txt contains random gene sets 
	along with gene sets corresponding to the block structure in 
	sample_input_data_file.txt (above). There are two columns in both of the 
	files corum_havig.txt and sample_gene_set_file.txt. On each line, the first 
	column is the name of a gene set and the second column is the Entrez ID of a 
	gene that belongs to the set.

	The files sample_gene_set_file.txt and sample_input_data_file.txt are meant 
	to be used together. The included scripts should discover all of the 
	built-in block structure of sample_input_data_file.txt. 

------------------------------
  II. How to run code
------------------------------

The commands to execute the scripts are as follows:

$ python find_nsclc_vulner.py "gene set filename" "k-means score filename"

For example, the "gene set filename" can be "sample_gene_set_file.txt"

$ cut -f3 "k-means score filename" | sort > "p-values filename"
$ Rscript nsclc_qvalue.R "p-values filename" "q-values filename"
$ python plot_nsclc_vulner.py "gene set filename" "k-means score filename" "FDR threshold"

------------------------------
  III. Copyright & Licensing
------------------------------

All code provided is freely available for academic purposes but may not be used 
commercially without permission.


------------------------------
  IV. References
------------------------------

[1] Storey JD and Tibshiran R. (2003) Statistcal significance for genome-wide 
experiments. Proceedings of the National Academy of Sciences, 100: 9440-9445.


------------------------------
  V. Contact info
------------------------------

Jonathan Young
jon.young@utexas.edu
