#!/usr/bin/env python

"""
Jonathan H. Young
jon.young@utexas.edu
9 July 2015

Find NSCLC genetic vulnerabilities from RNAi sensitivites.
This version uses Ckmeans.1d.dp, an R package for optimal 1D k-means.

Usage:
python find_nsclc_vulner.py <gene set filename> <output filename>
"""

import bisect
import math
import numpy
import sys
##from sklearn.cluster import KMeans
import readline
from rpy2.robjects import r


def get_sirna_entrez():
    """construct dictionary {Entrez ID:line #} (excluding header line)"""
    FILENAME = 'MuRiS.v8.1.csv'
    entrezCol = 0
    entrez2line = dict()
    fid = open(FILENAME)
    header = fid.readline()
    for i,line in enumerate(fid):
        fields = line.split(',')
        entrez2line[fields[entrezCol]] = i
    fid.close()
    return entrez2line


def read_into_mat(numLines):
    """create binary matrix based on siRNA robust z-scores"""
    CUTOFF = -3.0
    NUMCELLS = 13
    FILENAME = 'MuRiS.v8.1.csv'
    scoreMat = numpy.zeros((numLines,NUMCELLS), dtype=numpy.int)
    fid = open(FILENAME)
    header = fid.readline()
    for i,line in enumerate(fid):
        fields = line.split(',')
        if fields[2] != 'NA':  # HBEC30 column
            if float(fields[2]) <= CUTOFF:
                scoreMat[i,0] = 1
        for j in range(3, 26, 2):  # 2 columns (libraries) per NSCLC line
            if fields[j] != 'NA' and fields[j+1] != 'NA':
                if float(fields[j]) <= CUTOFF or float(fields[j+1]) <= CUTOFF:
                    scoreMat[i,(j-1)/2] = 1
            elif fields[j] != 'NA' and fields[j+1] == 'NA':
                if float(fields[j]) <= CUTOFF:
                    scoreMat[i,(j-1)/2] = 1
            elif fields[j] == 'NA' and fields[j+1] != 'NA':
                if float(fields[j+1]) <= CUTOFF:
                    scoreMat[i,(j-1)/2] = 1
            else:
                pass
    fid.close()
    return scoreMat


def read_zscore_mat(numLines):
    """create siRNA matrix of robust z-scores"""
    scoreMat = numpy.zeros((numLines,13))
    FILENAME = 'MuRiS.v8.1.csv'
    fid = open(FILENAME)
    header = fid.readline()
    for i,line in enumerate(fid):
        fields = line.split(',')
        if fields[2] != 'NA':  # HBEC30 column
            scoreMat[i,0] = float(fields[2])
        for j in range(3, 26, 2):  # 2 columns (libraries) per NSCLC line
            if fields[j] != 'NA' and fields[j+1] != 'NA':
                scoreMat[i,(j-1)/2] = min(float(fields[j]), float(fields[j+1]))
            elif fields[j] != 'NA' and fields[j+1] == 'NA':
                scoreMat[i,(j-1)/2] = float(fields[j])
            elif fields[j] == 'NA' and fields[j+1] != 'NA':
                scoreMat[i,(j-1)/2] = float(fields[j+1])
            else:
                pass
    fid.close()
    return scoreMat


def process_gene_set_file(filename):
    """Read in file containing gene sets (i.e. protein complexes) and return
    dictionary {set:[genes]}. Note file is assumed to be in a specific format;
    see included input files for example"""
    set2gene = dict()
    for line in open(filename):
        fields = line.split('\t')
        set2gene.setdefault(fields[0],[]).append(fields[1].strip())
    return set2gene


if __name__=="__main__":
    geneSetFilename = sys.argv[1]
    outputFilename = sys.argv[2]
    NUMPERM = 1000
    entrez2line = get_sirna_entrez()
    numLines = len(entrez2line)
    scoreMat = read_into_mat(numLines)
    numCols = scoreMat.shape[1]
    set2gene = process_gene_set_file(geneSetFilename)
    meansDiff = list()
    ##kmeans = KMeans(n_clusters=2, random_state=1)
    r('library(Ckmeans.1d.dp)')
    for geneSet in set2gene.keys():
        rowNums = list()
        for gene in set2gene[geneSet]:
            if gene in entrez2line:
                rowNums.append(entrez2line[gene])
        submatrix = scoreMat[rowNums,:]
        ##kmeans.fit(numpy.mean(submatrix, axis=0).reshape(numCols, 1))
        ##centroids = kmeans.cluster_centers_
        mu = numpy.mean(submatrix, axis=0)
        vecstr = 'c(' + ','.join([str(x) for x in mu]) + ')'
        r('result <- Ckmeans.1d.dp(' + vecstr + ', 2)')
        ##if centroids.size == 2:
        ##    meansDiff.append(math.fabs(centroids[1,0] - centroids[0,0]))
        ##else:
        ##    meansDiff.append(0.0)
        if r('length(result$centers)')[0] == 2:
            meansDiff.append(math.fabs(
                r('result$centers[2] - result$centers[1]')[0]))
        else:
            meansDiff.append(0.0)
    print('Now performing permutation test...')
    numpy.random.seed(1)  # set random seed for reproducibility
    randMat = numpy.copy(scoreMat)
    randDiff = numpy.zeros((NUMPERM, len(set2gene)))
    for i in range(NUMPERM):
        for j in range(numCols):
            numpy.random.shuffle(randMat[:,j])
        for jj,geneSet in enumerate(set2gene.keys()):
            rowNums = list()
            for gene in set2gene[geneSet]:
                if gene in entrez2line:
                    rowNums.append(entrez2line[gene])
            randSubmatrix = randMat[rowNums,:]
            ##kmeans.fit(numpy.mean(randSubmatrix, axis=0).reshape(numCols, 1))
            ##centroids = kmeans.cluster_centers_
            mu = numpy.mean(randSubmatrix, axis=0)
            vecstr = 'c(' + ','.join([str(x) for x in mu]) + ')'
            r('result <- Ckmeans.1d.dp(' + vecstr + ', 2)')
            ##if centroids.size == 2:
            ##    randDiff[i,jj] = math.fabs(centroids[1,0] - centroids[0,0])
            if r('length(result$centers)')[0] == 2:
                randDiff[i,jj] = math.fabs(
                        r('result$centers[2] - result$centers[1]')[0])
    randDiff.sort(axis=0)
    fid = open(outputFilename, 'w')
    for j,geneSet in enumerate(set2gene.keys()):
        x = NUMPERM - bisect.bisect_left(randDiff[:,j], meansDiff[j]) + 1
        y = NUMPERM + 1
        pval = float(x) / y
        fid.write(geneSet + '\t' + str(meansDiff[j]) + '\t' + str(pval) + '\n')
    fid.close()

