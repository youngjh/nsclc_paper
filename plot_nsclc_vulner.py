#!/usr/bin/env python

"""
Jonathan H. Young
jon.young@utexas.edu
7 November 2014

Based on RNAi sensitivity data, visualize black-white heatmap of NSCLC genetic
vulnerabilities satisfying a statistical significance cutoff

Usage:
python plot_nsclc_vulner.py <gene set file> <scores file> <threshold>
"""

import matplotlib.pyplot as plt
import numpy as np
import os
import sys


def get_sirna_entrez():
    """construct dictionary {Entrez ID:line #} (excluding header line)"""
    entrez2line = dict()
    entrezCol = 0
    fid = open('sample_input_data_file.txt')
    header = fid.readline()
    for i,line in enumerate(fid):
        fields = line.split(',')
        entrez2line[fields[entrezCol]] = i
    fid.close()
    return entrez2line


def read_into_mat(numLines):
    """create binary matrix based on siRNA robust z-scores"""
    CUTOFF = -3.0
    scoreMat = np.zeros((numLines, 13), dtype=np.int)
    fid = open('sample_input_data_file.txt')
    header = fid.readline()
    for i,line in enumerate(fid):
        fields = line.split(',')
        if fields[2] != 'NA':
            if float(fields[2]) <= CUTOFF:
                scoreMat[i,0] = 1
        for j in range(3, 26, 2):
            if fields[j] != 'NA' and fields[j+1] != 'NA':
                if float(fields[j]) <= CUTOFF or float(fields[j+1]) <= CUTOFF:
                    scoreMat[i,(j-1)/2] = 1
            elif fields[j] != 'NA' and fields[j+1] == 'NA':
                if float(fields[j]) <= CUTOFF:
                    scoreMat[i,(j-1)/2] = 1
            elif fields[j] == 'NA' and fields[j+1] != 'NA':
                if float(fields[j+1]) <= CUTOFF:
                    scoreMat[i,(j-1)/2] = 1
            else:
                pass
    fid.close()
    return scoreMat


def read_zscore_mat(numLines):
    """Create matrix for plotting based on siRNA robust z-scores.  
    Scores will be scaled between 0.0 and 1.0.  
    Any score <=-3.0 will be one color while any score >=0.0 will be 
    another color at the other end of the colormap.  Scores between 
    -3.0 and 0.0 will be colored according to the colormap.  
    The union of the z-scores is used."""
    lowCutoff = -3.0
    highCutoff = 0.0
    cutDiff = highCutoff - lowCutoff
    scoreMat = np.zeros((numLines, 13))
    fid = open('sample_input_data_file.txt')
    headerline = fid.readline()
    for i,line in enumerate(fid):
        fields = line.split(',')
        if fields[2] != 'NA':
            val = float(fields[2])
            if val > lowCutoff and val < highCutoff:
                scoreMat[i,0] = (val-lowCutoff)/cutDiff
            else:
                scoreMat[i,0] = (val <= lowCutoff)
        for j in range(3, 26, 2):
            if fields[j] != 'NA' and fields[j+1] != 'NA':
                val = min(float(fields[j]), float(fields[j+1]))
                if val > lowCutoff and val < highCutoff:
                    scoreMat[i,(j-1)/2] = (val-lowCutoff)/cutDiff
                else:
                    scoreMat[i,(j-1)/2] = (val<=lowCutoff)
            elif fields[j] != 'NA' and fields[j+1] == 'NA':
                val = float(fields[j])
                if val > lowCutoff and val < highCutoff:
                    scoreMat[i,(j-1)/2] = (val-lowCutoff)/cutDiff
                else:
                    scoreMat[i,(j-1)/2] = (val <= lowCutoff)
            elif fields[j] == 'NA' and fields[j+1] != 'NA':
                val = float(fields[j+1])
                if val > lowCutoff and val < highCutoff:
                    scoreMat[i,(j-1)/2] = (val-lowCutoff)/cutDiff
                else:
                    scoreMat[i,(j-1)/2] = (val <= lowCutoff)
            else:   #fields[j]=='NA' and fields[j+1]=='NA'
                pass    #scoreMat[i,(j-1)/2]=0.0
    fid.close()
    return scoreMat


def process_gene_set_file(filename):
    """Read in file containing gene sets (i.e. protein complexes) and return
    dictionary {set:[genes]}. Note file is assumed to be in a specific format;
    see included input files for example"""
    set2gene = dict()
    for line in open(filename):
        fields = line.split('\t')
        set2gene.setdefault(fields[0],[]).append(fields[1].strip())
    return set2gene


def get_header():
    """get and format header line from siRNA file"""
    headerLine = open('sample_input_data_file.txt').readline().split(',')
    lineout = list()
    lineout.append(headerLine[2][:-2])
    for j in range(3, 26, 2):
        lineout.append(headerLine[j][:-2])
    return lineout


def entrez_to_symbol():
    """dictionary to convert from Entrez ID to gene symbol"""
    entrez2sym = dict()
    fid = open('sample_input_data_file.txt')
    header = fid.readline()
    for line in fid:
        fields = line.split(',')
        entrez2sym[fields[0]] = fields[1]
    fid.close()
    return entrez2sym


def pathway_convert():
    """convert GO pathway symbols to name"""
    pwyconv = dict()
    fid = open('GO.terms_alt_ids', 'r')
    numHeaderLines = 11
    for i in xrange(numHeaderLines):
        line = fid.readline()
    for line in fid:
        fields = line.split('\t')
        pwyconv[fields[0]] = fields[2]
        if fields[1] != '':
            altids = fields[1].split(' ')
            for i in xrange(len(altids)):
                pwyconv[altids[i]] = fields[2]
    fid.close()
    return pwyconv


def plot_cluster_matrix(geneSetFile, scoreFile, threshold):
    """Gene set name should be in column 0 and p-value should be in column 2 
    of file with k-means scores and p-values."""
    sigGeneSets = list()
    MAXNAMELENGTH = 40  # maximum no. of chars in name of gene set
    for line in open(scoreFile):
        tokens = line.rstrip().split('\t')
        if float(tokens[2]) <= threshold:
            sigGeneSets.append(tokens[0])
    entrez2line = get_sirna_entrez()
    numLines = len(entrez2line)
    scoreMat = read_into_mat(numLines)
    set2genes = process_gene_set_file(geneSetFile)
    header = get_header()
    headerLength = len(header)
    entrez2sym = entrez_to_symbol()
    #pwyconv=pathway_convert()
    saveFolder = input('Enter name of folder to contain plots:\n')
    os.makedirs(saveFolder)
    for i,geneSet in enumerate(sigGeneSets):
        rowNums = list()
        geneLabels = list()
        for gene in set2genes[geneSet]:
            if gene in entrez2line:
                rowNums.append(entrez2line[gene])
                geneLabels.append(entrez2sym[gene])
        rownumsAndgenelabels = list(zip(rowNums,geneLabels))
        rownumsAndgenelabels.sort()
        sortedRowNums, geneLabels = zip(*rownumsAndgenelabels)
        submatrix = scoreMat[sortedRowNums,:]
        rowSum = np.sum(submatrix, axis=1)
        colSum = np.sum(submatrix, axis=0)
        sortRow = np.argsort(rowSum)[::-1]
        sortCol = np.argsort(colSum)[::-1]
        submatrix = submatrix[sortRow,:]
        submatrix = submatrix[:,sortCol]
        np.set_printoptions(precision=2, linewidth=100)
        sortedColLabels = np.take(header, sortCol)
        sortedRowLabels = np.take(geneLabels, sortRow)
        fig = plt.figure()
        plt.imshow(submatrix, cmap='Greys', aspect='auto', 
                interpolation='nearest')
        ax = plt.axes()
        plt.xticks(range(headerLength), sortedColLabels)
        plt.yticks(range(submatrix.shape[0]), sortedRowLabels)
        plt.tick_params(bottom='on', top='off', left='off', right='off')
        #plt.title(pwyconv[geneSet]+', ('+str(submatrix.shape[0])+' genes)')
        if len(geneSet) > MAXNAMELENGTH:
            geneSetTitle = geneSet[:MAXNAMELENGTH]
        else:
            geneSetTitle = geneSet
        plt.title(geneSetTitle + ', (' + str(submatrix.shape[0]) + ' genes)')
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize('x-small')
        fig.autofmt_xdate()
        #plt.show()
        plt.savefig(os.path.join(saveFolder, '') + 'gene_set_' + str(i) 
                + '.png', format='png')


if __name__=="__main__":
    geneSetFile = sys.argv[1]
    scoreFile = sys.argv[2]
    threshold = float(sys.argv[3])
    plot_cluster_matrix(geneSetFile, scoreFile, threshold)

